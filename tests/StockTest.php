<?php


namespace App\Tests;


use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class StockTest extends KernelTestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        DatabasePrimer::prime($kernel);

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->entityManager->close();
        $this->entityManager = null;
    }

    public function testIfStockRecordCanBeSavedSuccessfully()
    {
        //Arrange
        $stock = new Stock();
        $stock->setSymbol('AMZN');
        $stock->setShortName('Amazon Inc');
        $stock->setCurrency('USD');
        $stock->setExchangeName('Nasdaq');
        $stock->setRegion('US');
        $price = 1000;
        $previousClose = 1100;
        $priceChange = $price-$previousClose;
        $stock->setPrice($price);
        $stock->setPreviousClose($previousClose);
        $stock->setPriceChange($priceChange);
        $this->entityManager->persist($stock);
        //Act
        $this->entityManager->flush();
        $stockRepository = $this->entityManager->getRepository(Stock::class);
        $stockRecord= $stockRepository->findOneBy(['symbol' => 'AMZN']);
        //Assert
        $this->assertEquals('Amazon Inc',$stockRecord->getShortName());
        $this->assertEquals('AMZN',$stockRecord->getSymbol());
        $this->assertEquals('USD',$stockRecord->getCurrency());
        $this->assertEquals('Nasdaq',$stockRecord->getExchangeName());
        $this->assertEquals('US',$stockRecord->getRegion());
        $this->assertEquals($price,$stockRecord->getPrice());
        $this->assertEquals($previousClose,$stockRecord->getPreviousClose());
        $this->assertEquals($priceChange,$stockRecord->getPriceChange());
    }
}